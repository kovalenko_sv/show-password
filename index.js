"use strict";

const eye = document.querySelectorAll("i");
const form = document.querySelector("#form");

function setAtt(el, value) {
    el.setAttribute("type", value);
};

eye.forEach(el => {
    el.addEventListener("click", showPassword);
});

function showPassword(event) {
    const eve = event.target.classList;
    
    if (eve.contains("icon-password")) {
        const parent = event.target.closest("label");
        const input = parent.querySelector(".passwrd");

        if (input.type === "password") {
            setAtt(input, "text");
            eve.add("fa-eye-slash");
        } else {
            setAtt(input, "password");
            eve.replace("fa-eye-slash", "fa-eye");
        }
    } else {
        const allPass = form.querySelectorAll("input-wrapper");
        allPass.forEach(e => {
            const pass = e.querySelector(".passwrd");
            pass.setAttribute("password");
            const eyes = e.querySelector('.icon-password').classList.replace("fa-eye", "fa-eye-slash");

        });
    } 
}

const button = document.querySelector("button");
 
button.addEventListener("click", checkPass);
form.addEventListener("submit", checkPass);

function checkPass(e) {
    e.preventDefault();
    let one = document.getElementsByName("passwrd-one");
    let two = document.getElementsByName("passwrd-two");

    if (one[0].value === two[0].value && one[0].value !== "") {
          alert("You are welcome!");
    } else{
        const span = document.createElement("span");
        span.innerHTML = "You need to write the same symbols.";
        span.style.color = "red";
        span.style.marginBottom = "10px";
        button.before(span);
    };

    button.removeEventListener("click", checkPass);
}